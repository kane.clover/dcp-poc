const commitTemplate = `*{{#if scope}} **{{scope}}:**
{{~/if}} {{#if subject}}
  {{~subject}}
{{~else}}
  {{~header}}
{{~/if}}

{{~!-- commit link --}} {{#if @root.linkReferences~}}
  ([{{shortHash}}](
  {{~#if @root.repository}}
    {{~#if @root.host}}
      {{~@root.host}}/
    {{~/if}}
    {{~#if @root.owner}}
      {{~@root.owner}}/
    {{~/if}}
    {{~@root.repository}}
  {{~else}}
    {{~@root.repoUrl}}
  {{~/if}}/
  {{~@root.commit}}/{{hash}}))
{{~else}}
  {{~shortHash}}
{{~/if}}

{{~!-- commit references --}}
{{~#if references~}}
  , closes
  {{~#each references}}
    ([
    {{~#if this.owner}}
      {{~this.owner}}/
    {{~/if}}
    {{~this.repository}}#tp{{this.issue}}](https://auroratarget.tpondemand.com/entity/{{ this.issue }}))
  {{/each}}
 {{~else}}
   {{~newLineDueToMissingReferences}}
 {{~/if}}`;

module.exports = () =>
  require("conventional-changelog-angular").then(defaultAngularConfig => ({
    ...defaultAngularConfig,
    conventionalChangelog: {
      ...defaultAngularConfig.conventionalChangelog,
      writerOpts: {
        ...defaultAngularConfig.conventionalChangelog.writerOpts,
        commitPartial: commitTemplate,
        transform: (commit, context) => {
          let discard = true
          const issues = []

          commit.notes.forEach(note => {
            note.title = 'BREAKING CHANGES'
            discard = false
          })

          if (commit.type === 'feat') {
            commit.type = 'Features'
          } else if (commit.type === 'fix') {
            commit.type = 'Bug Fixes'
          } else if (commit.type === 'perf') {
            commit.type = 'Performance Improvements'
          } else if (commit.type === 'revert' || commit.revert) {
            commit.type = 'Reverts'
          } else if (discard) {
            return
          } else if (commit.type === 'docs') {
            commit.type = 'Documentation'
          } else if (commit.type === 'style') {
            commit.type = 'Styles'
          } else if (commit.type === 'refactor') {
            commit.type = 'Code Refactoring'
          } else if (commit.type === 'test') {
            commit.type = 'Tests'
          } else if (commit.type === 'build') {
            commit.type = 'Build System'
          } else if (commit.type === 'ci') {
            commit.type = 'Continuous Integration'
          }

          if (commit.scope === '*') {
            commit.scope = ''
          }

          if (typeof commit.hash === 'string') {
            commit.shortHash = commit.hash.substring(0, 7)
          }

          if (typeof commit.subject === 'string') {
            // Here is the magic with our referencing using #tpXXXXX
            commit.subject = commit.subject.replace(/#((tp)?[0-9]+)/g, (_, issue) => {
              issues.push(issue)
              return `[#${issue}](https://auroratarget.tpondemand.com/entity/${issue.replace(/tp/i, '')})`
            })

            if (context.host) {
              // User URLs.
              commit.subject = commit.subject.replace(/\B@([a-z0-9](?:-?[a-z0-9/]){0,38})/g, (_, username) => {
                if (username.includes('/')) {
                  return `@${username}`
                }

                return `[@${username}](${context.host}/${username})`
              })
            }
          }

          // remove references that already appear in the subject
          commit.references = commit.references.filter(reference => {
            if (issues.indexOf(reference.issue) === -1) {
              return true
            }

            return false
          })

          // modify references to tp issue to direct to the right issue on tp if stored in footer
          commit.references = commit.references.map(reference => ({
            ...reference,
            issue: reference.issue?.replace(/tp/i, "")
          }));

          commit.newLineDueToMissingReferences =
            commit.references?.length > 0 ? "" : "\n";

          return commit
        },
      }
    }
  }));
